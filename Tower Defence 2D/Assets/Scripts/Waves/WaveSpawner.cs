using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveSpawner : MonoBehaviour
{
    [SerializeField] GameObject enemyPrefab;
    [SerializeField] Transform enemyParent;
    [SerializeField] float timeBetweenEnemySpawns;
    [SerializeField] float timeBetweenWaves;

    [SerializeField] List<SpawnLocationObject> InactiveSpawnLocations = new List<SpawnLocationObject>();
    public List<SpawnLocationObject> ActiveSpawnLocations = new List<SpawnLocationObject>();

    public int numberOfEnemiesToSpawn;

    public int currentWave;

    private void Start()
    {
        IncreaseWave();
        AddNewSpawnLocationToActiveList();
        StartCoroutine(SpawnEnemy());
    }

    private void AddNewSpawnLocationToActiveList()
    {
        int random = Random.Range(0, InactiveSpawnLocations.Count);
        SpawnLocationObject selectedLocation = InactiveSpawnLocations[random];
        ActiveSpawnLocations.Add(selectedLocation);
        InactiveSpawnLocations.Remove(selectedLocation);
    }

    IEnumerator SpawnEnemy()
    {
        yield return new WaitForSeconds(timeBetweenEnemySpawns);
        if (numberOfEnemiesToSpawn > 0)
        {
            numberOfEnemiesToSpawn--;
            int random = Random.Range(0, ActiveSpawnLocations.Count);
            SpawnLocationObject spawnLocationObject = ActiveSpawnLocations[random];
            Vector2 spawnLocation = new Vector2(Random.Range(spawnLocationObject.xClampLocations.x, spawnLocationObject.xClampLocations.y), Random.Range(spawnLocationObject.yClampLocations.x, spawnLocationObject.yClampLocations.y));
            GameObject spawnedEnemy = Instantiate(enemyPrefab, spawnLocation, Quaternion.identity, enemyParent);
            EnemyAI ai = spawnedEnemy.GetComponent<EnemyAI>();
            ai.SetListOfWaypoints(spawnLocationObject.pathWaypoints);
            ai.SetLevel(currentWave);
            StartCoroutine(SpawnEnemy());
        }
        else if (numberOfEnemiesToSpawn == 0)
        {
            yield return new WaitForSeconds(timeBetweenWaves);
            IncreaseWave();
            StartCoroutine(SpawnEnemy());
        }
    }

    private void IncreaseWave()
    {
        currentWave++;
        numberOfEnemiesToSpawn = (currentWave + currentWave / 2);

        if (currentWave == 3)
        {
            AddNewSpawnLocationToActiveList();
        }

        if (currentWave == 5)
        {
            AddNewSpawnLocationToActiveList();
        }

        if (currentWave == 8)
        {
            AddNewSpawnLocationToActiveList();
        }
    }
}
