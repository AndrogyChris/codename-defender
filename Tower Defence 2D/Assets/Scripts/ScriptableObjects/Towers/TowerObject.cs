using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TowerType
{
    singleShot,
    concentrated
}

[CreateAssetMenu(fileName = "Tower Object", menuName = "Tower Data", order = 0)]
public class TowerObject : ScriptableObject
{
    public TowerType type;
    public float range;

    [Header("Sprites")]
    public List<Sprite> LevelSprites = new List<Sprite>();

    [Header("Damage")]
    public List<int> levelDamage = new List<int>();
    public float rateOfFire;
}
