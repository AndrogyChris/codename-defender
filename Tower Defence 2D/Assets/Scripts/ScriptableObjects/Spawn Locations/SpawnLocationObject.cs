using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Spawn Location", menuName = "Spawn Data", order = 0)]
public class SpawnLocationObject : ScriptableObject
{
    public Vector2 xClampLocations;
    public Vector2 yClampLocations;

    public List<Transform> pathWaypoints = new List<Transform>();
}
