using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class HealthBase : MonoBehaviour
{
    [Header("Health")]
    public int startingHealth;
    public int currentHealth;

    public virtual void SetHealth()
    {
        currentHealth = startingHealth;
    }

    public virtual void ReduceHealth(int incomingDamage)
    {
        currentHealth -= incomingDamage;

        if (currentHealth <= 0)
        {
            Die();
        }
    }

    public abstract void Die();
}
