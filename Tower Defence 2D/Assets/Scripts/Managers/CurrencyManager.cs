using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrencyManager : MonoBehaviour
{
    [SerializeField] int startingFunds;
    public int currentFunds;

    private void Start()
    {
        SetFunds();
    }

    private void SetFunds()
    {
        currentFunds = startingFunds;
    }

    public void IncreaseFunds(int incommingFunds)
    {
        currentFunds += incommingFunds;
    }

    public void DecreaseFunds(int leavingFunds)
    {
        currentFunds -= leavingFunds;
    }
}
