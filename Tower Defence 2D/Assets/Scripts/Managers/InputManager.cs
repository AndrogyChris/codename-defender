using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Events;

public class InputManager : MonoBehaviour
{
    public static UnityEvent<Vector2> OnMousePos = new UnityEvent<Vector2>();
    public static UnityEvent OnLeftClick = new UnityEvent();
    public static UnityEvent OnRightClick = new UnityEvent();

    TDActions actions;

    private void Awake()
    {
        actions = new TDActions();

        actions.player.mousePos.performed += MousePosInput;
        actions.player.leftClick.performed += x => OnLeftClick.Invoke();
        actions.player.rightClick.performed += x => OnRightClick.Invoke();
    }

    private void MousePosInput(InputAction.CallbackContext ctx)
    {
        Vector2 mousePos = ctx.ReadValue<Vector2>();
        OnMousePos.Invoke(mousePos);
    }

    private void OnEnable()
    {
        actions.Enable();
    }
    private void OnDisable()
    {
        actions.Disable();
    }
}
