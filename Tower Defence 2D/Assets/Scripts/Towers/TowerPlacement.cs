using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TowerPlacement : MonoBehaviour
{
    [Header("Tower Placement")]
    [SerializeField] GameObject towerPrefab;
    [SerializeField] TowerObject rangedTowerSO;
    [SerializeField] TowerObject meleeTowerSO;

    [Header("Wall Placement")]
    [SerializeField] GameObject wallPrefab;

    [Header("Placeables Parent")]
    [SerializeField] GameObject TowersParent;

    [Header("Select Tower")]
    [SerializeField] LayerMask towerLayer;
    [SerializeField] GameObject upgradeTowerButton;
    public Tower highlightedTower;

    [Header("Tower Costs")]
    [SerializeField] int towerCost;
    [SerializeField] int wallCost;
    [SerializeField] public int upgradeCost;

    GameObject towerToPlace;
    Tower towerComponent;
    Wall wallComponent;
    Vector2 mousePosition;
    Camera cam;
    RaycastHit2D hit;
    CurrencyManager currencyManager;

    private void Awake()
    {
        cam = Camera.main;
        currencyManager = FindObjectOfType<CurrencyManager>();
    }

    private void OnEnable()
    {
        //Add Listeners from Input Manager
        InputManager.OnMousePos.AddListener(DetermineMousePos);
        InputManager.OnLeftClick.AddListener(LeftClickInput);
        InputManager.OnRightClick.AddListener(RightClickInput);
    }
    private void OnDisable()
    {
        //Remove Listeners 
        InputManager.OnMousePos.RemoveListener(DetermineMousePos);
        InputManager.OnLeftClick.RemoveListener(LeftClickInput);
        InputManager.OnRightClick.RemoveListener(RightClickInput);
    }

    private void FixedUpdate()
    {
        MovePlacingTowerToMouse(); //If there is a tower to place se its position to the mouse's
    }

    private void MovePlacingTowerToMouse()
    {
        if (towerToPlace != null)
        {
            towerToPlace.transform.localPosition = mousePosition;
        }
    }

    private void LeftClickInput()
    {
        if (EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }
        else
        {
            if (towerToPlace != null)
            {
                if (towerComponent != null)
                {
                    if (towerComponent.canPlace)
                    {
                        currencyManager.DecreaseFunds(towerCost);
                        towerComponent.OnTowerPlaced();
                        towerToPlace = null;
                    }
                }

                if (wallComponent != null)
                {
                    if (wallComponent.canPlace)
                    {
                        currencyManager.DecreaseFunds(wallCost);
                        wallComponent.OnWallPlaced();
                        towerToPlace = null;
                    }
                }
            }
            else
            {
                hit = Physics2D.Raycast(mousePosition, Vector2.zero, 0, towerLayer);
                if (hit)
                {
                    if (highlightedTower == null)
                    {
                        highlightedTower = hit.collider.GetComponent<Tower>();
                        highlightedTower.ToggleTowerSelected(true);
                        if (highlightedTower.currentLevel == highlightedTower.maxLevel)
                        {
                            upgradeTowerButton.SetActive(false);
                        }
                        else
                        {
                            upgradeTowerButton.SetActive(true);
                        }
                    }
                    else
                    {
                        highlightedTower.ToggleTowerSelected(false);
                        highlightedTower = hit.collider.GetComponent<Tower>();
                        highlightedTower.ToggleTowerSelected(true);
                        if (highlightedTower.currentLevel == highlightedTower.maxLevel)
                        {
                            upgradeTowerButton.SetActive(false);
                        }
                        else
                        {
                            upgradeTowerButton.SetActive(true);
                        }
                    }
                }
                else
                {
                    DeselectTower();
                }
            }
        }
    }

    private void DeselectTower()
    {
        if (highlightedTower != null)
        {
            upgradeTowerButton.SetActive(false);
            highlightedTower.ToggleTowerSelected(false);
            highlightedTower = null;
        }
    }

    private void RightClickInput()
    {
        //Destroy tower
        if (towerToPlace != null)
        {
            Destroy(towerToPlace);
            towerToPlace = null;
        }
    }

    private void DetermineMousePos(Vector2 incommingMousePos)
    {
        mousePosition = cam.ScreenToWorldPoint(incommingMousePos);
    }

    public void PlaceWall()
    {
        if (towerToPlace == null && currencyManager.currentFunds >= wallCost)
        {
            DeselectTower();
            towerComponent = null;
            towerToPlace = Instantiate(wallPrefab, mousePosition, Quaternion.identity, TowersParent.transform);
            wallComponent = towerToPlace.GetComponent<Wall>();
        }
    }

    public void PlaceRangedTower()
    {
        SelectTowerToPlace(rangedTowerSO); //Choose Ranged tower to place
    }

    public void PlaceMeleeTower()
    {
        SelectTowerToPlace(meleeTowerSO); //Choose Melee tower to place
    }

    private void SelectTowerToPlace(TowerObject selectedTower)
    {
        if (towerToPlace == null && currencyManager.currentFunds >= towerCost)
        {
            DeselectTower();
            wallComponent = null;
            towerToPlace = Instantiate(towerPrefab, mousePosition, Quaternion.identity, TowersParent.transform);
            towerComponent = towerToPlace.GetComponent<Tower>();
            towerComponent.towerObject = selectedTower;
            towerComponent.SetTowerStats();
        }
    }

    public void UpgradeTower()
    {
        if (highlightedTower != null)
        {
            int cost = highlightedTower.currentLevel * upgradeCost;
            if (highlightedTower.currentLevel < highlightedTower.maxLevel && cost <= currencyManager.currentFunds)
            {
                currencyManager.DecreaseFunds(cost);
                highlightedTower.currentLevel++;
                highlightedTower.SetTowerStats();

                if (highlightedTower.currentLevel == highlightedTower.maxLevel)
                {
                    upgradeTowerButton.SetActive(false);
                }
            }
        }
    }
}
