using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Tower : MonoBehaviour
{
    enum towerStates
    {
        placing,
        shooting
    }

    [Header("Tower State")]
    [SerializeField] towerStates currentState;

    [Header("Tower Objects")]
    public TowerObject towerObject;
    [SerializeField] SpriteRenderer turretSprite;
    [SerializeField] GameObject rangeObject;
    [SerializeField] GameObject positivePlacement;
    [SerializeField] GameObject negativePlacement;
    [SerializeField] LayerMask avoidLayers;

    [Header("Tower Stats")]
    public int currentLevel;
    public int maxLevel;
    public int currentDamage;
    public float currentRange;
    public float currentTimeBetweenShots;

    public bool canPlace;
    bool stillInTrigger;


    [Header("Enemies")]
    public List<GameObject> listOfEnemies = new List<GameObject>();
    public GameObject currentTarget;

    Coroutine shootingCoroutine;

    BoxCollider2D boxCollider;
    CircleCollider2D circleCollider;

    private void Awake()
    {
        //Set components
        boxCollider = transform.GetComponent<BoxCollider2D>();
        circleCollider = rangeObject.GetComponent<CircleCollider2D>();
    }

    private void Start()
    {
        canPlace = true;
    }

    private void Update()
    {
        if (currentState == towerStates.shooting)
        {
            RotateTowardsTarget(); //Rotate turret sprite toward target
        }
    }

    private void RotateTowardsTarget()
    {
        //Convert v2 direction of enemy position to an angle and apply it to the turret sprite
        if (currentTarget != null)
        {
            Vector2 v = currentTarget.transform.position - turretSprite.gameObject.transform.position;
            float angle = Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg - 90f;
            turretSprite.gameObject.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Change behaviour depending on state
        switch (currentState)
        {
            case towerStates.placing:
                CheckIfEnteringUnplacable(collision);
                break;

            case towerStates.shooting:
                AddEnemyToList(collision);
                break;

            default:
                break;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        //Change behaviour depending on state
        switch (currentState)
        {
            case towerStates.placing:
                CheckIfExitingUnplaceable(collision);
                break;

            case towerStates.shooting:
                RemoveEnemyFromList(collision);
                break;

            default:
                break;
        }
    }

    //Read TowerObject ScriptableObject to determine the Tower's stats
    public void SetTowerStats()
    {
        //Set sprite and Damage depending on level
        turretSprite.sprite = towerObject.LevelSprites[currentLevel - 1];
        currentDamage = towerObject.levelDamage[currentLevel - 1];

        //only do these if is in placing state
        if (currentState == towerStates.placing)
        {
            //Set scale of enemy detection to match the range set in the ScriptableObject
            currentRange = towerObject.range * 2;
            rangeObject.transform.localScale = new Vector3(currentRange, currentRange, currentRange);

            //Invert the RateOfFire to determine time between shots
            currentTimeBetweenShots = 1 / towerObject.rateOfFire;

            //Set enemy trigger to be small so that it can grow to detect on placement
            circleCollider.radius = 0.01f;

            maxLevel = towerObject.levelDamage.Count;
        }
    }

    public void OnTowerPlaced()
    {
        positivePlacement.SetActive(false); //Set range radius false on placement 
        currentState = towerStates.shooting; //switch state
        ExpandDetectionCollider(); //set trigger to true size to detect enemies
    }

    public void ToggleTowerSelected(bool isSelected)
    {
        if (isSelected)
        {
            positivePlacement.SetActive(true);
        }
        else
        {
            positivePlacement.SetActive(false);
        }
    }

    private void ExpandDetectionCollider()
    {
        circleCollider.radius = 0.5f;
    }

    //Wait for time between shots to shoot at an enemy
    IEnumerator ShootAtEnemy()
    {
        yield return new WaitForSeconds(currentTimeBetweenShots);
        if (currentTarget != null)
        {
            DealDamage(); //directly deal damage through the target's HealthBase
            shootingCoroutine = StartCoroutine(ShootAtEnemy());
        }
    }

    private void DealDamage()
    {
        //Reduce health of current target
        currentTarget.GetComponent<EnemyAI>().ReduceHealth(currentDamage);
    }

    private void CheckIfEnteringUnplacable(Collider2D col)
    {
        //when entering an unplaceable location make it so tower can't be placed
        if (col.CompareTag("Path") || col.CompareTag("Border") || col.CompareTag("Tower"))
        {
            canPlace = false;
            //change display radius to whether the tower can or can't be placed
            positivePlacement.SetActive(false); 
            negativePlacement.SetActive(true);
        }
    }

    private void CheckIfExitingUnplaceable(Collider2D col)
    {
        //check if tower is exiting an unplaceable location
        if (col.CompareTag("Path") || col.CompareTag("Border") || col.CompareTag("Tower"))
        {
            //Box cast checks if the tower has actually left the unplaceable location
            //Disables and reenables box collider so it is not part of the check
            boxCollider.enabled = false;
            stillInTrigger = Physics2D.BoxCast(transform.position, boxCollider.size, 0f, Vector2.down, 0, avoidLayers);
            boxCollider.enabled = true;

            if (!stillInTrigger)
            {
                canPlace = true;
                //change display radius to positive
                positivePlacement.SetActive(true);
                negativePlacement.SetActive(false);
            }
        }
    }

    private void AddEnemyToList(Collider2D col)
    {
        if (col.CompareTag("Enemy"))
        {
            //adds entered enemy to a list of targets
            listOfEnemies.Add(col.gameObject);
            if (currentTarget == null)
            {
                FindClosestEnemy();
                shootingCoroutine = StartCoroutine(ShootAtEnemy());
            }
        }
    }

    private void RemoveEnemyFromList(Collider2D col)
    {
        if (col.CompareTag("Enemy"))
        {
            //removes enemy from possible targets
            listOfEnemies.Remove(col.gameObject);
            if (!listOfEnemies.Contains(currentTarget) && listOfEnemies.Count > 0)
            {
                // find new target is the old one is removed
                FindClosestEnemy();
            }
            else if (listOfEnemies.Count == 0)
            {
                // stop coroutine if there is no target
                StopCoroutine(shootingCoroutine);
                currentTarget = null;
            }
        }
    }

    private void FindClosestEnemy()
    {
        // move through all enemies in the list and determine the closest
        float currentDistanceToTower = Mathf.Infinity;
        foreach (var enemy in listOfEnemies)
        {
            float distanceToTower = Vector3.Distance(transform.position, enemy.transform.position);
            if (distanceToTower < currentDistanceToTower)
            {
                currentDistanceToTower = distanceToTower;
                currentTarget = enemy;
            }
        }
    }
}
