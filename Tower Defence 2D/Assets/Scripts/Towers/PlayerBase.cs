using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBase : HealthBase
{
    SceneLoader sceneLoader;
    [SerializeField] GameObject baseObject;

    private void Awake()
    {
        sceneLoader = FindObjectOfType<SceneLoader>();
    }

    private void Start()
    {
        SetHealth();
    }

    public override void Die()
    {
        StartCoroutine(DeathCoroutine());
    }

    IEnumerator DeathCoroutine()
    {
        baseObject.SetActive(false);

        yield return new WaitForSeconds(2);

        sceneLoader.DisplayGameOver();
    }
}
