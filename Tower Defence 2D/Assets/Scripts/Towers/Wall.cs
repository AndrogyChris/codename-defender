using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : HealthBase
{
    [Header("Wall Object")]
    [SerializeField] GameObject positivePlacement;
    [SerializeField] GameObject negativePlacement;
    [SerializeField] LayerMask avoidLayers;

    bool placing;
    public bool canPlace;
    bool stillInTrigger;

    CircleCollider2D circleCollider;

    private void Awake()
    {
        circleCollider = GetComponent<CircleCollider2D>();
    }
    private void Start()
    {
        placing = true;
        SetHealth();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (placing)
        {
            CheckIfEnteringUnplacable(collision);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (placing)
        {
            CheckIfExitingUnplaceable(collision);
        }
    }

    private void CheckIfEnteringUnplacable(Collider2D col)
    {
        if (col.CompareTag("Background") || col.CompareTag("Border") || col.CompareTag("Tower"))
        {
            canPlace = false;
            positivePlacement.SetActive(false);
            negativePlacement.SetActive(true);
        }
    }

    private void CheckIfExitingUnplaceable(Collider2D col)
    {
        if (col.CompareTag("Background") || col.CompareTag("Border") || col.CompareTag("Tower"))
        {
            circleCollider.enabled = false;
            stillInTrigger = Physics2D.CircleCast(transform.position, circleCollider.radius, Vector2.down, 0, avoidLayers);
            circleCollider.enabled = true;

            if (!stillInTrigger)
            {
                canPlace = true;
                positivePlacement.SetActive(true);
                negativePlacement.SetActive(false);
            }
        }
    }

    public void OnWallPlaced()
    {
        positivePlacement.SetActive(false);
        placing = false;
        gameObject.layer = 12;
    }

    public override void Die()
    {
        Destroy(gameObject);
    }
}
