using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ButtonUI : MonoBehaviour
{
    [SerializeField] GameObject upgradeButton;

    TowerPlacement towerPlacement;

    private void Awake()
    {
        towerPlacement = FindObjectOfType<TowerPlacement>();
    }

    private void LateUpdate()
    {
        if (upgradeButton.activeSelf)
        {
            upgradeButton.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = $"Upgrade \n- ${towerPlacement.highlightedTower.currentLevel * towerPlacement.upgradeCost}";
        }
    }
}
