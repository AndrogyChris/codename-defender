using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class WaveCurrencyUI : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI currencyText;
    [SerializeField] TextMeshProUGUI waveText;

    CurrencyManager currencyManager;
    WaveSpawner waveSpawner;

    private void Awake()
    {
        currencyManager = FindObjectOfType<CurrencyManager>();
        waveSpawner = FindObjectOfType<WaveSpawner>();
    }

    private void LateUpdate()
    {
        UpdateCurrency();
        UpdateWave();
    }

    private void UpdateCurrency()
    {
        currencyText.text = $"$ {currencyManager.currentFunds}";
    }

    private void UpdateWave()
    {
        waveText.text = $"Wave: {waveSpawner.currentWave}";
    }
}
