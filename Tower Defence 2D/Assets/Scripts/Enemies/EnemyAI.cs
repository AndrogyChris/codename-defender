using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
//using System;

public class EnemyAI : HealthBase
{
    [Header("Stats")]
    public int level;
    [SerializeField] int baseDamage;
    [SerializeField] float rateOfFire;
    public int currentDamage;


    [Header("Pathfinding")]
    [SerializeField] List<Transform> majorWaypointsList = new List<Transform>();
    [SerializeField] float nextWaypointDistance;
    [SerializeField] float stoppingDistance;
    Transform target;
    int currentMajorWaypointIndex;
    int currentWaypoint;

    [Header("Movement")]
    [SerializeField] float moveSpeed;
    [SerializeField] Transform sprite;

    [Header("Attack")]
    public GameObject currentTarget;
    public List<GameObject> attackTargets = new List<GameObject>();

    bool reachedEndOfPath;

    Path path;
    Seeker seeker;
    Rigidbody2D rb;

    Coroutine attackCoroutine;

    CurrencyManager currencyManager;

    enum states
    {
        move,
        attack
    }

    [SerializeField] states currentState;

    private void Awake()
    {
        //Assign Components
        seeker = GetComponent<Seeker>();
        rb = GetComponent<Rigidbody2D>();

        currencyManager = FindObjectOfType<CurrencyManager>();
    }

    private void Start()
    {
        UpdatePath(); //Choose first target and start pathfinding
    }

    private void FixedUpdate()
    {
        switch (currentState) //switch between states
        {
            case states.move:
                MoveState();
                break;

            case states.attack:
                AttackState();
                break;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<HealthBase>() != null)
        {
            AddTargetToList(collision);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<HealthBase>() != null)
        {
            RemoveTargetFromList(collision);
        }
    }

    private void AddTargetToList(Collider2D col)
    {
        attackTargets.Add(col.gameObject);
        if (currentTarget == null)
        {
            FindClosestEnemy();
            attackCoroutine = StartCoroutine(AttackTarget());
        }
    }

    private void RemoveTargetFromList(Collider2D col)
    {
        attackTargets.Remove(col.gameObject);
        if (!attackTargets.Contains(currentTarget) && attackTargets.Count > 0)
        {
            FindClosestEnemy();
        }
        else
        {
            if (attackCoroutine != null)
            {
                StopCoroutine(attackCoroutine);
            }
            currentTarget = null;
            currentState = states.move;
        }
    }

    private void FindClosestEnemy()
    {
        float currentDistanceToTower = Mathf.Infinity;
        foreach (var enemy in attackTargets)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
            if (distanceToEnemy < currentDistanceToTower)
            {
                currentDistanceToTower = distanceToEnemy;
                currentTarget = enemy;
            }
        }
        currentState = states.attack;
    }

    IEnumerator AttackTarget()
    {
        yield return new WaitForSeconds(1 / rateOfFire);
        if (currentTarget != null)
        {
            currentTarget.GetComponent<HealthBase>().ReduceHealth(currentDamage);
            attackCoroutine = StartCoroutine(AttackTarget());
        }
    }

    private void MoveState()
    {
        CheckToSelectNewMajorWaypoint(); //if the enemy has reaches its waypoint choose the next on in waypoints list
        MoveAlongPath(); //use pathfinding and rigidbody to move along astar path
        RotateToFaceTarget(rb.velocity); //rotate enemy sprite to face the direction of it's velocity
    }

    private void AttackState()
    {
        Vector2 targetDirection = currentTarget.transform.position - transform.position;
        RotateToFaceTarget(targetDirection);
    }

    private void UpdatePath()
    {
        target = majorWaypointsList[currentMajorWaypointIndex];

        if (seeker.IsDone() && target != null)
        {
            seeker.StartPath(rb.position, target.position, OnPathComplete);
        }
    }

    private void OnPathComplete(Path p)
    {
        if (!p.error)
        {
            path = p;
            currentWaypoint = 0;
        }
    }

    private void CheckToSelectNewMajorWaypoint()
    {
        if (Vector3.Distance(transform.position, target.position) <= stoppingDistance)
        {
            currentMajorWaypointIndex++;

            if (currentMajorWaypointIndex > majorWaypointsList.Count - 1)
            {
                //currentState = states.attack;
            }
            UpdatePath();
        }
    }

    private void MoveAlongPath()
    {
        if (path == null)
            return;

        if (currentWaypoint >= path.vectorPath.Count)
        {
            reachedEndOfPath = true;
            return;
        }
        else
        {
            reachedEndOfPath = false;
        }

        Vector2 direction = ((Vector2)path.vectorPath[currentWaypoint] - rb.position).normalized;
        Vector2 force = direction * moveSpeed * Time.deltaTime;
        rb.AddForce(force);

        float distance = Vector2.Distance(rb.position, path.vectorPath[currentWaypoint]);

        if (distance < nextWaypointDistance)
        {
            currentWaypoint++;
        }
    }

    private void RotateToFaceTarget(Vector2 targetVector)
    {
        Vector2 v = targetVector;
        float angle = Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg - 90f;
        sprite.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

    // Die called from HealthBase
    public override void Die()
    {
        int moneyDrop = Random.Range(2, 15);
        currencyManager.IncreaseFunds(moneyDrop);
        Destroy(gameObject);
    }

    public void SetListOfWaypoints(List<Transform> waypointsToSet)
    {
        majorWaypointsList = waypointsToSet;
    }

    public void SetLevel(int wave)
    {
        level = wave;
        currentDamage = baseDamage + level / 2;
        currentHealth = startingHealth + level / 2;
    }
}
