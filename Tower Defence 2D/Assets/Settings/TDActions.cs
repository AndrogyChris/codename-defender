// GENERATED AUTOMATICALLY FROM 'Assets/Settings/TDActions.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @TDActions : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @TDActions()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""TDActions"",
    ""maps"": [
        {
            ""name"": ""player"",
            ""id"": ""10fae227-a584-4f46-b8a2-ec519b9db628"",
            ""actions"": [
                {
                    ""name"": ""mousePos"",
                    ""type"": ""Value"",
                    ""id"": ""7d0944fc-210b-4815-901d-c5f9d967e353"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""leftClick"",
                    ""type"": ""Button"",
                    ""id"": ""3ea019e2-cacf-4c57-a468-c5d429d450ab"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""rightClick"",
                    ""type"": ""Button"",
                    ""id"": ""6127d252-46d0-47c2-8c67-cbb626ad34ab"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""13433930-fbf4-4da1-97dc-599426e196d5"",
                    ""path"": ""<Mouse>/position"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""mousePos"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b9012388-9302-418f-97af-4d95df24c61a"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""leftClick"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""83c83046-7739-4d4c-a87f-bf9da5a4c9d2"",
                    ""path"": ""<Mouse>/rightButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""rightClick"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // player
        m_player = asset.FindActionMap("player", throwIfNotFound: true);
        m_player_mousePos = m_player.FindAction("mousePos", throwIfNotFound: true);
        m_player_leftClick = m_player.FindAction("leftClick", throwIfNotFound: true);
        m_player_rightClick = m_player.FindAction("rightClick", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // player
    private readonly InputActionMap m_player;
    private IPlayerActions m_PlayerActionsCallbackInterface;
    private readonly InputAction m_player_mousePos;
    private readonly InputAction m_player_leftClick;
    private readonly InputAction m_player_rightClick;
    public struct PlayerActions
    {
        private @TDActions m_Wrapper;
        public PlayerActions(@TDActions wrapper) { m_Wrapper = wrapper; }
        public InputAction @mousePos => m_Wrapper.m_player_mousePos;
        public InputAction @leftClick => m_Wrapper.m_player_leftClick;
        public InputAction @rightClick => m_Wrapper.m_player_rightClick;
        public InputActionMap Get() { return m_Wrapper.m_player; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(PlayerActions set) { return set.Get(); }
        public void SetCallbacks(IPlayerActions instance)
        {
            if (m_Wrapper.m_PlayerActionsCallbackInterface != null)
            {
                @mousePos.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMousePos;
                @mousePos.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMousePos;
                @mousePos.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnMousePos;
                @leftClick.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLeftClick;
                @leftClick.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLeftClick;
                @leftClick.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnLeftClick;
                @rightClick.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnRightClick;
                @rightClick.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnRightClick;
                @rightClick.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnRightClick;
            }
            m_Wrapper.m_PlayerActionsCallbackInterface = instance;
            if (instance != null)
            {
                @mousePos.started += instance.OnMousePos;
                @mousePos.performed += instance.OnMousePos;
                @mousePos.canceled += instance.OnMousePos;
                @leftClick.started += instance.OnLeftClick;
                @leftClick.performed += instance.OnLeftClick;
                @leftClick.canceled += instance.OnLeftClick;
                @rightClick.started += instance.OnRightClick;
                @rightClick.performed += instance.OnRightClick;
                @rightClick.canceled += instance.OnRightClick;
            }
        }
    }
    public PlayerActions @player => new PlayerActions(this);
    public interface IPlayerActions
    {
        void OnMousePos(InputAction.CallbackContext context);
        void OnLeftClick(InputAction.CallbackContext context);
        void OnRightClick(InputAction.CallbackContext context);
    }
}
