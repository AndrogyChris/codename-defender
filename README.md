# Codename - Defender

This is a simple tower defense game.

Click on buttons to buy towers. Left click to place a tower and right click to delete a selected one.

Kill enemies to earn more money.

Click on an already placed tower to upgrade it.

Enjoy!
